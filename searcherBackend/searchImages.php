<?php 

    header('Access-Control-Allow-Origin: *');
    include "./includes/db_includer.php";

    $search = trim($_GET['search']);

    $tags = explode(' ', $search);

    $query_string = 'SELECT * FROM `images` WHERE ';

    foreach($tags as $tag)
    {
        $query_string .= " `tags` LIKE '%$tag%' OR";
    }
    $query_string = substr($query_string, 0, strlen($query_string) - 3);

    $db_answer = mysqli_query($con, $query_string);
    $result_rows_count = mysqli_num_rows($db_answer);

    $result_images = array();

    if($result_rows_count > 0)
    {
        while($record = mysqli_fetch_assoc($db_answer))
        {
            $author_id = $record['author_id'];
            $author_get_query = mysqli_query($con, 
            "SELECT `name` AS `author_name`, `icon_url` AS `author_url` FROM `accounts` WHERE `id` = '$author_id'");

            $record = array_merge($record, mysqli_fetch_assoc($author_get_query));

            array_push($result_images, $record);
        }
    }
    else
    {
        echo 'no reults';
    }

    echo json_encode($result_images);
?>