<?php
    header('Access-Control-Allow-Origin: *');
    include "./includes/db_includer.php";
        
    $name = $_POST['name'];
    $password = $_POST['password'];

    $db_answer = mysqli_query($con, "SELECT * FROM `accounts` WHERE `name` = '$name'");
    //OOP: $db_answerOOP = $con->query('SQL query');
    //PDO: $db_answerPDO = $con2->query('SQL query'); or exec() with prepare()
    // u can use $db_answerPDO in foreach

    if ($db_answer == false) {
        echo 'DB error';
    }else{
        $account = mysqli_fetch_assoc($db_answer);
        //OOP: $db_answerOOP->fetch_assoc();
        $response = array('id' => 0,
                          'rights' => 2,
                          'name_is_correct' => true,
                          'password_is_correct' => true,
                          'url' => '');

        if($account != null){
            if($account['password'] != $password){
                $response['password_is_correct'] = false;
            }else{
                $response['id'] = $account['id'];
                $response['rights'] = $account['access_rights'];
                $response['url'] = $account['icon_url'];
            }
        }else {
            $response['name_is_correct'] = false;
        }
        echo json_encode($response);
    }
?>