import {React, useState} from 'react'
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import {useNavigate} from 'react-router-dom'
import { logIn } from './LoginForm'


const RegistrationForm = function({userInfoSetters}) {

  const navigate = useNavigate();

  const [name, setName] = useState('');
  const [pwd, setPwd] = useState('');
  const [setIcon, setSetIcon] = useState(false);
  const [iconURL, setIconURL] = useState('https://upload.wikimedia.org/wikipedia/commons/9/99/Sample_User_Icon.png');

  const [underNameMsg, setUnderNameMsg] = useState('');
  const [underPwdMsg, setUnderPwdMsg] = useState('');

  function submit(e) {
    e.preventDefault();

    let canNavigate = true;
    if(name === '') {
      setUnderNameMsg('name must contain at least 1 character!');
      canNavigate = false;
    }else{
      setUnderNameMsg('');
    }
    if(pwd === '') {
      setUnderPwdMsg('password must contain at least 1 character!');
      canNavigate = false;
    }else{
      setUnderPwdMsg('');
    }

    if(canNavigate){
      var server = 'http://searcherBackend/registration.php';

      var formdata = new FormData();
      formdata.append('name', name);
      formdata.append('password', pwd);
      formdata.append('url', iconURL);

      fetch(server, {
        method : 'POST',
        header : {'Content-Type' : 'application/x-www-form-urlencoded'},
        body : formdata
      })
      .then(response => response.json())
      .then(response => {
        if(response.server_has_problem) {
          alert('trouble with server');
        }else{
          if(response.login_already_exist) {
            setUnderNameMsg('this username already exists');
          }else{

            logIn(name, pwd)
            .then(response => {
              if(!response.name_is_correct){
                setUnderNameMsg('this loggin is incorrect, please check it');
              }else{
                setUnderNameMsg('');
                if(!response.password_is_correct){
                  setUnderPwdMsg('your password is incorrect');
                }else{
                  setUnderPwdMsg('');
                  userInfoSetters.setIconSrc(response.url);
                  localStorage.setItem('iconSrc', JSON.stringify(response.url));
      
                  userInfoSetters.setUserName(name);
                  localStorage.setItem('userName', JSON.stringify(name));
      
                  userInfoSetters.setUserId(response.id);
                  localStorage.setItem('userId', JSON.stringify(response.id));
      
                  userInfoSetters.setUserRights(response.rights);
                  localStorage.setItem('userRights', JSON.stringify(response.rights));
                  navigate('/');
                }
              }
            });
          }
        }
      });
    }
  }

	return (

		<div className="center-bordered wpercent40 sidePad">
  		<h1>Registration</h1>
  		<Form>
        <Form.Group className="mb-3" controlId="formBasicName">
          <Form.Label>Your nickname</Form.Label>
          <Form.Control type="text" placeholder="Enter nickname" onChange={(e)=>{setName(e.currentTarget.value)}} />
          {underNameMsg == ''
            ? null
            : <p className="red">{underNameMsg}</p>
          }
          
        </Form.Group>

        <Form.Group className="mb-3" controlId="formBasicPassword">
          <Form.Label>Password</Form.Label>
          <Form.Control type="password" placeholder="Password" onChange={(e)=>{setPwd(e.currentTarget.value)}} />
          {underPwdMsg == ''
            ? null
            : <p className="red">{underPwdMsg}</p>
          }
        </Form.Group>

        <Form.Group className="mb-3" controlId="formBasicCheckbox">
          <Form.Check type="checkbox" label="set icon" onClick={(e)=>{setSetIcon(!setIcon)}}/>
        </Form.Group>
        {setIcon?
          <Form.Group className="mb-3" controlId="formBasicPassword">
          <div className="width100perc center">  
            <img src = {iconURL} className="center ico_sized"></img>
          </div>
          <Form.Label>URL</Form.Label>
          <Form.Control type="text" placeholder="url of your icon" onChange={(e)=>{setIconURL(e.currentTarget.value)}} />
          </Form.Group>
          : null
        }

        <Button variant="primary" type="submit" onClick={submit}>
          Submit
        </Button>
  </Form>
	</div>
	);
}

export default RegistrationForm;