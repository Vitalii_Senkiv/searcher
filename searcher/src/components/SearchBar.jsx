import React, { useState } from 'react'
import { useNavigate, useNavigationType } from 'react-router-dom';
import "../styles/SearchBar.css"
//
const SearchBar = () =>
{
    return (
        <form action='/'>
            <div props>
                <input autocomplete="off" type='text' name='search'  placeholder='Enter tags of images' className='search-bar color-search'></input>
            </div>
        </form>
    );
}

export default SearchBar;