import { Button } from 'react-bootstrap';
import React, { useState } from 'react'
import ImageComponent from './ImageComponent';
import { useNavigate } from 'react-router-dom';

const Home = function({userInfo}) {
	const serverURL = 'http://searcherBackend/searchImages.php';
	const navigate = useNavigate();

	const [URLParams, setURLParams] = useState(new URLSearchParams(window.location.search));
	const [previousStr, setPreviousStr] = useState('');
	const [serverResp, setServerResp] = useState([]);


	if (URLParams.get('search')) {
		if(URLParams.get('search') != previousStr){
			setPreviousStr(URLParams.get('search'));

			fetch(serverURL + '?' + URLParams)
			.then(respose => respose.json())
			.then(response =>{
				setServerResp(response);
			});
		}
	}

	return (
		<div >
			{(userInfo.userRights <= 1)
			?	<Button title='add new image' variant="light" className="addButton" onClick={()=>navigate('/add')}>+</Button>
			:	null
			}
			{(!URLParams.get('search'))
			?<div className="center-bordered wpercent40">
				<h1 className='center'>Welcome in searcher!</h1>
				<div className='center'>
					here will be shown results of your search
					<img className='center wpercent30' src='https://thumbs.gfycat.com/MistyBleakDrongo-size_restricted.gif'/>
				</div>
			  </div>
			:serverResp.length > 0
				? serverResp.map(image =>
					<div className="center-bordered wpercent40">
						<ImageComponent image={image} userInfo={userInfo} images={serverResp} setImages={setServerResp}/>
					</div>
					)
				: <div className="center-bordered wpercent40">
					<h1 className='center'>Sorry...</h1>
					<div className='center'>
						There are not such images(<br/>
						But you can become first who publish this image:)
						<img className='center wpercent30' src='https://thumbs.gfycat.com/MistyBleakDrongo-size_restricted.gif'/>
					</div>
				  </div>
			}
		</div>
	);
}

export default Home;