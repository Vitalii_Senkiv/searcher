import { Badge, Button, Form } from "react-bootstrap";
import trashBinIcon from "../trash.png"
import editIcon from "../editico.png"
import { useState } from "react";

const EditImageComponent = ({image, images, setImages, setUnderEdit}) =>{

    const [name, setName] = useState(image.name);
    const [src, setSrc] = useState(image.src);
    const [tagsStr, setTagsStr] = useState(image.tags);

    const url = 'http://searcherBackend/editPicture.php';

    function change(e)
    {
        e.preventDefault();

        var formData = new FormData();
        formData.append('picture_id', image.id);
        formData.append('name', name);
        formData.append('source', src);
        formData.append('tags', tagsStr);

        fetch(url, {
            method: 'POST',
            header: {"Content-Type": 'application/x-www-form-urlencoded'},
            body: formData
        })
        .then(response=>response.json())
        .then(response=>{
            if(response.changed == true) {
                image.name = name;
                image.src = src;
                image.tags = tagsStr;
                setImages(images);
                setUnderEdit(false);
            }
        });
    }

    return (
        <div>
            {/* {(userInfo.userRights === 0 || image.author_id === userInfo.userId)} */}
            {/* <h1 className="center paddingTop">{image.name}</h1> */}
            <Form>
                <Form.Group className="my-3" controlId="formBasicName">
                    <Form.Label className="mx-3">Picture name</Form.Label>
                    <Form.Control type="text" placeholder="Enter picture name" value={name} onChange={(e)=>{setName(e.currentTarget.value)}}/>
                </Form.Group>

                <Form.Group className="my-2" controlId="formBasicPassword">
                    <div className="center"><img className = "width100perc center" src = {src}/></div>
                    <Form.Label className="mx-3">Picture source(url)</Form.Label>
                    <Form.Control type="text" placeholder="URL" value={src} onChange={(e)=>{setSrc(e.currentTarget.value)}}/>        
                </Form.Group> 

                <Form.Group className="my-2" controlId="formBasicPassword">
                    <Form.Label className="mx-3">Tags</Form.Label>
                    <Form.Control type="text" placeholder="Enter tags" value={tagsStr} onChange={(e)=>{setTagsStr(e.currentTarget.value)}}/>        
                </Form.Group> 

                <div className='text-center mt-3s'>
                    <Button variant="primary" type="submit" onClick={change}>
                        Apply
                    </Button>
                </div>
            </Form>
        </div>
    )
}

export default EditImageComponent;