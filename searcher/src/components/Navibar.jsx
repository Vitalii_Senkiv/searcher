import React from 'react'
import {Navbar, Nav, Button, Image} from 'react-bootstrap'
import {Link, useLocation, useNavigate} from 'react-router-dom'
import 'bootstrap/dist/css/bootstrap.min.css';
import SearchBar from './SearchBar';

import logo from "../safari.svg"

const NaviBar = ({userInfoSetters, userInfo}) => {
	const navigate = useNavigate();
	const location = useLocation();

	return(
		<Navbar collapseOnSelect expand="lg" variant="light" className='px-2 color-nav'>
			<Navbar.Brand className="pad cursor_pointer" href='/'>
                <Image src={logo} width="35" height="35"></Image> 
            </Navbar.Brand>
            {   location.pathname === "/" 
                ? <Navbar.Brand className="cursor_pointer" href='/'> Search </Navbar.Brand>
                : null
            }
            {   location.pathname === "/" 
                ? <SearchBar/>
                : null
            }
			<Navbar.Toggle aria-controls="responsive-navbar-nav"/>
                <Navbar.Collapse className="justify-content-end">
                <Nav>
                    { userInfo.userName === ''
                        ? <Button variant="primary" className="me-2" onClick={()=>{navigate('/login');}}>Log in</Button>
                        : <Nav>
                            <Navbar.Text className="me-2">user:{userInfo.userName}</Navbar.Text>
                            <Image roundedCircle src={userInfo.iconSrc} className="icon me-2" />
                            <Button variant='danger' className="me-2" href='/' onClick={()=>{userInfoSetters.signOut();}}>Sign out</Button>
                            </Nav>
                    }

                </Nav>
                </Navbar.Collapse>
		</Navbar>
	)
}

export default NaviBar;