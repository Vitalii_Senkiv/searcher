import { Badge } from "react-bootstrap";
import trashBinIcon from "../trash.png"
import editIcon from "../editico.png"
import { useState } from "react";
import EditImageComponent from "./EditImageComponent";

const ImageComponent = ({image, userInfo, images, setImages}) =>{
    var tags = image.tags.split(' ');
	const [underEdit, setUnderEdit] = useState(false);


    function removePicture(pictureId)
    {
        const url = 'http://searcherBackend/removePicture.php'

        var formData = new FormData();
        formData.append('picture_id', pictureId);

        fetch(url, {
            method: 'POST',
            header: {"Content-Type": 'application/x-www-form-urlencoded'},
            body: formData
        })
        .then(response=>response.json())
        .then(response=>{
            if(response.removed){
                setImages(images.filter((current)=> current != image));
            }
        });
    }

    return (
        <div>
            {!underEdit
            ?
                <span>
                {(userInfo.userRights == 0 || image.author_id == userInfo.userId)
                ?  <div className="">
                        <img className = "sign cursor_pointer float-right right-top signmarg" src = {trashBinIcon} onClick={()=>{removePicture(image.id)}}/>
                        <img className = "sign cursor_pointer float-left right-top signmarg" src = {editIcon} onClick={()=>{setUnderEdit(true)}}/>
                    </div>
                    :null
                }
                <h1 className="center paddingTop">{image.name}</h1>
                    
                <div className="center"><img className = "width100perc center" src = {image.src}/></div>
                {tags.map(tag =>
                    <Badge pill bg="secondary">{tag}</Badge>
                )}
                
                <div>
                    <br/>
                    <img className = "icon round iconmarg" src = {image.author_url}/>
                    {image.author_id == userInfo.userId
                    ?<span>You</span>
                    :image.author_name}
                </div>
                </span>
                :<EditImageComponent image={image} setImages={setImages} images={images} setUnderEdit={setUnderEdit}/>
            }
        </div>
    )
}

export default ImageComponent;