import {React, useEffect, useState} from 'react'
import {Form, Button, Modal, Image} from 'react-bootstrap'

const AddPicturePage = function({userInfo}) {

    const [name, setName] = useState('');
    const [source, setSource] = useState('');
    const [tags, setTags] = useState('');
    const [greenMessage, setGreenMessage] = useState('');
    const [redMessage, setRedMessage] = useState('');

    const backendPageURL = 'http://searcherBackend/addImage.php';

    function submit(e) {
        e.preventDefault();

        var formData = new FormData();
        formData.append('name', name);
        formData.append('source', source);
        formData.append('tags', tags);
        formData.append('author_id', userInfo.userId)

        fetch(backendPageURL, {
            method: 'POST',
            header: {"Content-Type": 'application/x-www-form-urlencoded'},
            body: formData
        })
        .then(response=>response.json())
        .then(response=>{
            if(response.added == true) {
                setRedMessage('');
                setGreenMessage(name + " added successfully");
            }
            else{
                setRedMessage(name + " adding failure");
                setGreenMessage('');
            }
        });
    }

    return (
        <div>
            <div className='center-bordered wpercent40 sidePad'>
                <Form>
                    <Form.Group className="my-3" controlId="formBasicName">
                        <Form.Label>Picture name</Form.Label>
                        <Form.Control type="text" placeholder="Enter picture name" onChange={(e)=>{setName(e.currentTarget.value)}}/>
                        <Form.Text className="text-muted">
                            This name of picture will be shown to other users
                        </Form.Text>
                    </Form.Group>

                    <Form.Group className="my-2" controlId="formBasicPassword">
                        <div className='center'><Image className='width100perc center' src={source}></Image></div>
                        <Form.Label>Picture source(url)</Form.Label>
                        <Form.Control type="text" placeholder="URL" onChange={(e)=>{setSource(e.currentTarget.value)}}/>  
                        <Form.Text className="text-muted">
                            URL source of picture
                        </Form.Text>       
                    </Form.Group> 

                    <Form.Group className="my-2" controlId="formBasicPassword">
                        <Form.Label>Tags</Form.Label>
                        <Form.Control type="text" placeholder="Enter tags" onChange={(e)=>{setTags(e.currentTarget.value)}}/>  
                        <Form.Text className="text-muted">
                            Is used for search
                        </Form.Text>       
                    </Form.Group> 

                    <div className='text-center mt-3s'>
                        <Button variant="primary" type="submit" onClick={submit}>
                            Add
                        </Button>
                        <p className="red">{redMessage}</p>
                        <p className="green">{greenMessage}</p>
                    </div>
                </Form>
            </div>
        </div>
    );
}

export default AddPicturePage;