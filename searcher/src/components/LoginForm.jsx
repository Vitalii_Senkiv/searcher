import {React, useState} from 'react'
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import {useNavigate} from 'react-router-dom'

export async function logIn(name, password)
{
  var url = 'http://searcherBackend/login.php';

  var formdata = new FormData();
  formdata.append('name', name);
  formdata.append('password', password);

  var res = await fetch(url, {
  method : 'POST',
  header : {'Content-Type' : 'application/x-www-form-urlencoded'},
  body : formdata
  });

  return await res.json();
}

const LoginForm = function({userInfoSetters}) {

  const navigate = useNavigate();

  const [name, setName] = useState('');
  const [pwd, setPwd] = useState('');

  const [underNameMsg, setUnderNameMsg] = useState('');
  const [underPwdMsg, setUnderPwdMsg] = useState('');

  function submit(e) {
    e.preventDefault();

    let canNavigate = true;
    if(name === '') {
      setUnderNameMsg('please, enter your nickname');
      canNavigate = false;
    }else{
      setUnderNameMsg('');
    }
    if(pwd === '') {
      setUnderPwdMsg('please, enter your password');
      canNavigate = false;
    }else{
      setUnderPwdMsg('');
    }

    if(canNavigate){
      logIn(name, pwd)
      .then(response => {
        if(!response.name_is_correct){
          setUnderNameMsg('this loggin is incorrect, please check it');
        }else{
          setUnderNameMsg('');
          if(!response.password_is_correct){
            setUnderPwdMsg('your password is incorrect');
          }else{
            setUnderPwdMsg('');
            userInfoSetters.setIconSrc(response.url);
            localStorage.setItem('iconSrc', JSON.stringify(response.url));

            userInfoSetters.setUserName(name);
            localStorage.setItem('userName', JSON.stringify(name));

            userInfoSetters.setUserId(response.id);
            localStorage.setItem('userId', JSON.stringify(response.id));

            userInfoSetters.setUserRights(response.rights);
            localStorage.setItem('userRights', JSON.stringify(response.rights));
            navigate('/');
          }
        }
      });
    }
  }

	return (

		<div className="center-bordered wpercent40 sidePad">
  		<h1>Log in</h1>
  		<Form>
        <Form.Group className="mb-3" controlId="formBasicName">
          <Form.Label>Your name</Form.Label>
          <Form.Control type="text" placeholder="Enter name" onChange={(e)=>{setName(e.currentTarget.value)}} />
          {underNameMsg == ''
            ? null
            : <p className="red">{underNameMsg}</p>
          }
          
        </Form.Group>

        <Form.Group className="mb-3" controlId="formBasicPassword">
          <Form.Label>Password</Form.Label>
          <Form.Control type="password" placeholder="Password" onChange={(e)=>{setPwd(e.currentTarget.value)}} />
          {underPwdMsg == ''
            ? null
            : <p className="red">{underPwdMsg}</p>
          }
        </Form.Group>

        <Form.Group className="mb-3">
          <a href='/registration'>Have not account yet?</a>
        </Form.Group>

        <Button variant="primary" type="submit" onClick={submit}>
          Submit
        </Button>
  </Form>
	</div>
	);
}

export default LoginForm;