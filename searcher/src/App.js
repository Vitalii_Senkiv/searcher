import './App.css';
import {BrowserRouter as Router, Routes, Route, Link} from 'react-router-dom'
import Home from '../src/components/Home'
import LoginForm from './components/LoginForm';
import RegistrationForm from './components/RegistrationForm';
import { useEffect, useState } from 'react';
import NaviBar from './components/Navibar';
import AddPicturePage from './components/AddPicturePage';


function App() {
  
  const [userName, setUserName] = useState('');
  const [iconSrc, setIconSrc] = useState('');//https://hope.be/wp-content/uploads/2015/05/no-user-image.gif
  const [userId, setUserId] = useState(0); // unknown user
  const [userRights, setUserRights] = useState(2); // no rights
  
  function signOut()
  {
    setUserName('');
    setIconSrc('');
    setUserId(0);
    setUserRights(2);
    localStorage.clear();
  }

var userInfoSetters = { setUserName, setIconSrc, setUserId, setUserRights, signOut}
var userInfo = { userName, iconSrc, userId, userRights }

  useEffect( () => {
    const userName = JSON.parse(localStorage.getItem('userName'));
    if(userName) { setUserName(userName); }
  }, []);

  useEffect( () => {
    const iconSrc = JSON.parse(localStorage.getItem('iconSrc'));
    if(iconSrc) { setIconSrc(iconSrc); }
  }, []);

  useEffect( () => {
    const userId = JSON.parse(localStorage.getItem('userId'));
    if(userId) { setUserId(userId); }
  }, []);

  useEffect( () => {
    const userRights = JSON.parse(localStorage.getItem('userRights'));
    if(userRights) { setUserRights(userRights); }
  }, []);


  return (
    <div className="App">
      <Router>
        <NaviBar userInfoSetters={userInfoSetters} userInfo={userInfo}></NaviBar>
        <Routes>
          <Route exact path="/" element={<Home userInfo={userInfo}/>} />
          <Route path="/login" element={<LoginForm userInfoSetters = {userInfoSetters}/>} />
          <Route path="/registration" element={<RegistrationForm userInfoSetters = {userInfoSetters}/>} />
          <Route path="/add" element={<AddPicturePage userInfo={userInfo}/>} />
        </Routes>
      </Router>
    </div>
  );
}

export default App;
